//
//  ViewController.swift
//  Password
//
//  Created by yigitgunes on 26.03.2018.
//  Copyright © 2018 yigitgunes. All rights reserved.
//

import UIKit
import CoreMotion
import AudioToolbox


class ViewController: UIViewController {
    

    @IBOutlet weak var classNameLabel: UILabel!
    
    @IBOutlet weak var info: UILabel!
    
    @IBOutlet weak var secondLine: UILabel!
    var gyroscopeArray = [[Double]]()
    var accelerometerArray = [[Double]]()
    var magnetometerArray = [[Double]]()
    var gyroscopeDictionary : [[String : Any]] = [[:]]
    var magnetometerDictionary : [[String : Any]] = [[:]]
    var accelerometerDictionary : [[String : Any]] = [[:]]
    var model = finalMotion()
    var counter : Double = 0
    var motionManager = CMMotionManager()
    var featureDict: [String: Double] = [:]

    @IBOutlet weak var save: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        save.isEnabled = false
        classNameLabel.text = " "
        info.text = " "
        secondLine.text = " "
//        guard let output = try? model.prediction(bedroom: 1, bath: 1, size: 95, price: 4) else {
//            let b = 34
//            return
//        }
//        let a  = output.classProbability.count
//        let bn = 3
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: Actions
    
    @IBAction func hodl(_ sender: UIButton) {
    }
    
    @IBAction func record(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            startRecording()
        }
        if sender.state == .ended {
            stopRecording()
        }
    }
    
    @IBAction func save(_ sender: UIButton) {
        save.isEnabled = false
        // extract features
        // use model
        let motionResult = extractFeatures()
        let userResult = extractFeaturesUser()
        let bothResult = extractFeaturesBoth()
        let user = String(userResult)
        let indexPerson = user.index(user.startIndex, offsetBy: 7)
        let both = String(bothResult)
        let indexBothMotion = both.index(both.startIndex, offsetBy: 6)
        let indexBothUser = both.index(both.startIndex, offsetBy: 5)

        info.text = "Motion Classification \n Motion: \(String(motionResult))"
        classNameLabel.text = "User Classification \n User: user\(user[indexPerson])"
        secondLine.text = "Motion and User Classification \n User: user\(both[indexBothUser]) Motion: motion\(both[indexBothMotion])"
        
    }
    
    func extractFeatures() -> String {
        
        let extract = FeatureExtractor()

        var gyroMax = extract.max(data: gyroscopeDictionary)
        var gyroMin = extract.min(data: gyroscopeDictionary)
        var gyroMean = extract.mean(data: gyroscopeDictionary)
        var gyroMad = extract.meanAbsDeviation(data: gyroscopeDictionary)
        var gyroStd = extract.standardDeviation(data: gyroscopeDictionary)
        var accMax = extract.max(data: accelerometerDictionary)
        var accMin = extract.min(data: accelerometerDictionary)
        var accMean = extract.mean(data: accelerometerDictionary)
        var accMad = extract.meanAbsDeviation(data: accelerometerDictionary)
        var accStd = extract.standardDeviation(data: accelerometerDictionary)
        var magMax = extract.max(data: magnetometerDictionary)
        var magMin = extract.min(data: magnetometerDictionary)
        var magMean = extract.mean(data: magnetometerDictionary)
        var magMad = extract.meanAbsDeviation(data: magnetometerDictionary)
        var magStd = extract.standardDeviation(data: magnetometerDictionary)
//        let arr = ["gyro", "acc", "magne"]
//        let a = ["min", "max", "mean", "mad", "std"]
//        for j in arr{
//            let str = "xyz"
//            for i in str {
//                featureDict[String(j) + String(i)] =
//            }
//        }
        guard let output = try? model.prediction(gyro_mean_x: gyroMean["x"]!, gyro_mean_y: gyroMean["y"]!,gyro_mean_z:gyroMean["z"]!,gyro_max_x: gyroMax["x"]!,gyro_max_y: gyroMax["y"]!,gyro_max_z: gyroMax["z"]!,gyro_min_x: gyroMin["x"]!,gyro_min_y: gyroMin["y"]!,gyro_min_z: gyroMin["z"]!,gyro_std_x: gyroStd["x"]!,gyro_std_y: gyroStd["y"]!,gyro_std_z: gyroStd["z"]!,gyro_mad_x: gyroMad["x"]!,gyro_mad_y: gyroMad["y"]!,gyro_mad_z: gyroMad["z"]!,acc_mean_x: accMean["x"]!,acc_mean_y: accMean["y"]!,acc_mean_z: accMean["z"]!,acc_max_x: accMax["x"]!,acc_max_y: accMax["y"]!,acc_max_z: accMax["z"]!,acc_min_x: accMin["x"]!,acc_min_y: accMin["y"]!,acc_min_z: accMin["z"]!,acc_std_x: accStd["x"]!,acc_std_y:accStd["y"]!,acc_std_z: accStd["z"]!,acc_mad_x: accMad["x"]!,acc_mad_y: accMad["y"]!,acc_mad_z: accMad["z"]!,mag_mean_x: magMean["x"]!,mag_mean_y: magMean["y"]!,mag_mean_z: magMean["z"]!,mag_max_x: magMax["x"]!,mag_max_y: magMax["y"]!,mag_max_z: magMax["z"]!,mag_min_x: magMin["x"]!,mag_min_y: magMin["y"]!,mag_min_z: magMin["z"]!,mag_std_x: magStd["x"]!,mag_std_y: magStd["y"]!,mag_std_z:magStd["z"]!,mag_mad_x: magMad["x"]!,mag_mad_y: magMad["y"]!,mag_mad_z: magMad["z"]!)
            else {
                    return ""
                }
        let a  = output.class_
       // let b = output.classProbability
               // let c = b[1]
        
        return a
    
        
    }
    
    func extractFeaturesBoth() -> String {
        
        let extract = FeatureExtractor()
        
        var gyroMax = extract.max(data: gyroscopeDictionary)
        var gyroMin = extract.min(data: gyroscopeDictionary)
        var gyroMean = extract.mean(data: gyroscopeDictionary)
        var gyroMad = extract.meanAbsDeviation(data: gyroscopeDictionary)
        var gyroStd = extract.standardDeviation(data: gyroscopeDictionary)
        var accMax = extract.max(data: accelerometerDictionary)
        var accMin = extract.min(data: accelerometerDictionary)
        var accMean = extract.mean(data: accelerometerDictionary)
        var accMad = extract.meanAbsDeviation(data: accelerometerDictionary)
        var accStd = extract.standardDeviation(data: accelerometerDictionary)
        var magMax = extract.max(data: magnetometerDictionary)
        var magMin = extract.min(data: magnetometerDictionary)
        var magMean = extract.mean(data: magnetometerDictionary)
        var magMad = extract.meanAbsDeviation(data: magnetometerDictionary)
        var magStd = extract.standardDeviation(data: magnetometerDictionary)
        //        let arr = ["gyro", "acc", "magne"]
        //        let a = ["min", "max", "mean", "mad", "std"]
        //        for j in arr{
        //            let str = "xyz"
        //            for i in str {
        //                featureDict[String(j) + String(i)] =
        //            }
        //        }
        let bothModel = finalBoth()
        guard let output = try? bothModel.prediction(gyro_mean_x: gyroMean["x"]!, gyro_mean_y: gyroMean["y"]!,gyro_mean_z:gyroMean["z"]!,gyro_max_x: gyroMax["x"]!,gyro_max_y: gyroMax["y"]!,gyro_max_z: gyroMax["z"]!,gyro_min_x: gyroMin["x"]!,gyro_min_y: gyroMin["y"]!,gyro_min_z: gyroMin["z"]!,gyro_std_x: gyroStd["x"]!,gyro_std_y: gyroStd["y"]!,gyro_std_z: gyroStd["z"]!,gyro_mad_x: gyroMad["x"]!,gyro_mad_y: gyroMad["y"]!,gyro_mad_z: gyroMad["z"]!,acc_mean_x: accMean["x"]!,acc_mean_y: accMean["y"]!,acc_mean_z: accMean["z"]!,acc_max_x: accMax["x"]!,acc_max_y: accMax["y"]!,acc_max_z: accMax["z"]!,acc_min_x: accMin["x"]!,acc_min_y: accMin["y"]!,acc_min_z: accMin["z"]!,acc_std_x: accStd["x"]!,acc_std_y:accStd["y"]!,acc_std_z: accStd["z"]!,acc_mad_x: accMad["x"]!,acc_mad_y: accMad["y"]!,acc_mad_z: accMad["z"]!,mag_mean_x: magMean["x"]!,mag_mean_y: magMean["y"]!,mag_mean_z: magMean["z"]!,mag_max_x: magMax["x"]!,mag_max_y: magMax["y"]!,mag_max_z: magMax["z"]!,mag_min_x: magMin["x"]!,mag_min_y: magMin["y"]!,mag_min_z: magMin["z"]!,mag_std_x: magStd["x"]!,mag_std_y: magStd["y"]!,mag_std_z:magStd["z"]!,mag_mad_x: magMad["x"]!,mag_mad_y: magMad["y"]!,mag_mad_z: magMad["z"]!)
            else {
                return ""
        }
        let a  = output.class_
        // let b = output.classProbability
        // let c = b[1]
        
        return a
        
        
    }
    
    
    func extractFeaturesUser() -> String {
        
        let extract = FeatureExtractor()
        
        var gyroMax = extract.max(data: gyroscopeDictionary)
        var gyroMin = extract.min(data: gyroscopeDictionary)
        var gyroMean = extract.mean(data: gyroscopeDictionary)
        var gyroMad = extract.meanAbsDeviation(data: gyroscopeDictionary)
        var gyroStd = extract.standardDeviation(data: gyroscopeDictionary)
        var accMax = extract.max(data: accelerometerDictionary)
        var accMin = extract.min(data: accelerometerDictionary)
        var accMean = extract.mean(data: accelerometerDictionary)
        var accMad = extract.meanAbsDeviation(data: accelerometerDictionary)
        var accStd = extract.standardDeviation(data: accelerometerDictionary)
        var magMax = extract.max(data: magnetometerDictionary)
        var magMin = extract.min(data: magnetometerDictionary)
        var magMean = extract.mean(data: magnetometerDictionary)
        var magMad = extract.meanAbsDeviation(data: magnetometerDictionary)
        var magStd = extract.standardDeviation(data: magnetometerDictionary)
        //        let arr = ["gyro", "acc", "magne"]
        //        let a = ["min", "max", "mean", "mad", "std"]
        //        for j in arr{
        //            let str = "xyz"
        //            for i in str {
        //                featureDict[String(j) + String(i)] =
        //            }
        //        }
        let userModel = finalUser()
                guard let output = try? userModel.prediction(gyro_mean_x: gyroMean["x"]!, gyro_mean_y: gyroMean["y"]!,gyro_mean_z:gyroMean["z"]!,gyro_max_x: gyroMax["x"]!,gyro_max_y: gyroMax["y"]!,gyro_max_z: gyroMax["z"]!,gyro_min_x: gyroMin["x"]!,gyro_min_y: gyroMin["y"]!,gyro_min_z: gyroMin["z"]!,gyro_std_x: gyroStd["x"]!,gyro_std_y: gyroStd["y"]!,gyro_std_z: gyroStd["z"]!,gyro_mad_x: gyroMad["x"]!,gyro_mad_y: gyroMad["y"]!,gyro_mad_z: gyroMad["z"]!,acc_mean_x: accMean["x"]!,acc_mean_y: accMean["y"]!,acc_mean_z: accMean["z"]!,acc_max_x: accMax["x"]!,acc_max_y: accMax["y"]!,acc_max_z: accMax["z"]!,acc_min_x: accMin["x"]!,acc_min_y: accMin["y"]!,acc_min_z: accMin["z"]!,acc_std_x: accStd["x"]!,acc_std_y:accStd["y"]!,acc_std_z: accStd["z"]!,acc_mad_x: accMad["x"]!,acc_mad_y: accMad["y"]!,acc_mad_z: accMad["z"]!,mag_mean_x: magMean["x"]!,mag_mean_y: magMean["y"]!,mag_mean_z: magMean["z"]!,mag_max_x: magMax["x"]!,mag_max_y: magMax["y"]!,mag_max_z: magMax["z"]!,mag_min_x: magMin["x"]!,mag_min_y: magMin["y"]!,mag_min_z: magMin["z"]!,mag_std_x: magStd["x"]!,mag_std_y: magStd["y"]!,mag_std_z:magStd["z"]!,mag_mad_x: magMad["x"]!,mag_mad_y: magMad["y"]!,mag_mad_z: magMad["z"]!)
            else {
                return ""
        }
        let a  = output.class_
        // let b = output.classProbability
        // let c = b[1]
        
        return a
        
        
    }
    
    func startRecording() {
//        gyroscopeArray.removeAll()
//        accelerometerArray.removeAll()
//        magnetometerArray.removeAll()
        classNameLabel.text = " "
        info.text = " "
        secondLine.text = " "
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        gyroscopeDictionary.removeAll()
        accelerometerDictionary.removeAll()
        magnetometerDictionary.removeAll()
        
        motionManager.startGyroUpdates(to: OperationQueue.current!) { (data,error) in
            let timestamp = Date().timeIntervalSince1970 * 1000
            
            if let gyro = data {
                let gyroElement = ["timestamp" : timestamp, "x" : gyro.rotationRate.x, "y" : gyro.rotationRate.y, "z" : gyro.rotationRate.z]
                //self.gyroscopeArray.append(gyroElement)
               self.gyroscopeDictionary.append(gyroElement)
            }
        }
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (data,error) in
            let timestamp = Date().timeIntervalSince1970 * 1000
            
            if let acc = data {
                let accElement = ["timestamp" : timestamp, "x" : acc.acceleration.x, "y" : acc.acceleration.y, "z" : acc.acceleration.z]
                //self.accelerometerArray.append(accElement)
                self.accelerometerDictionary.append(accElement)
                
            }
        }
        counter = 0
        motionManager.startMagnetometerUpdates(to: OperationQueue.current!){(data,error) in
            let timestamp = Date().timeIntervalSince1970 * 1000
            
            if let mag = data {
                //let magElement = [timestamp, mag.magneticField.x, mag.magneticField.y, mag.magneticField.z]
                //self.magnetometerArray.append(magElement)
                let magData = ["timestamp" : timestamp, "x" : mag.magneticField.x, "y" : mag.magneticField.y, "z" : mag.magneticField.z]
                self.magnetometerDictionary.append(magData)
                self.counter += 1
            }
        }
    }
    
    func stopRecording(){
        motionManager.stopGyroUpdates()
        motionManager.stopAccelerometerUpdates()
        motionManager.stopMagnetometerUpdates()
        save.isEnabled = true
        //extractFeatures()
    }
    
    
    
}

