//
//  FeatureExtractor.swift
//  Password
//
//  Created by yigitgunes on 26.03.2018.
//  Copyright © 2018 yigitgunes. All rights reserved.
//

import Foundation

class FeatureExtractor {
    
    func mean(data: [[String : Any]]) -> [String : Double] {
        var meanData : [String : Double] = [:]
        var total : Double = 0
        let str = "xyz"
        for char in str {
            total = 0
            for i in data {
                total += ( i[String(char)] as! Double)
            }
            meanData[String(char)] = total / Double(data.count)
        }
        
        return meanData
    }
    
    func max (data: [[String : Any]]) -> [String : Double]  {
        var max : [String : Double] = [:]
        
        let str = "xyz"
        for char in str {
            max[String(char)] = (data[0])[String(char)] as? Double
            for i in data {
                let value = i[String(char)] as! Double
                if (value > max[String(char)]!) {
                    max[String(char)] = value
                }
            }
        }
     return max
    }
    
    func min (data: [[String : Any]]) -> [String : Double]  {
        var min : [String : Double] = [:]
        
        let str = "xyz"
        for char in str {
            min[String(char)] = (data[0])[String(char)] as? Double
            for i in data {
                let value = i[String(char)] as! Double
                if (value < min[String(char)]!) {
                    min[String(char)] = value
                }
            }
        }
        return min
    }
    
    func meanAbsDeviation(data: [[String : Any]]) -> [String : Double] {
        var absSum :[String: Double] = [:]
        var mad : [String: Double] = [:]

        let meanData = mean(data: data)
        let str = "xyz"
        for char in str {
            absSum[String(char)] = 0
            for i in data {
                let value = i[String(char)] as! Double
                (absSum[String(char)]! ) += abs(value - meanData[String(char)]!)
            }
            mad[String(char)] = absSum[String(char)]! / Double(data.count)
        }
        
        return mad
    }
    
    func standardDeviation (data: [[String : Any]]) -> [String : Double] {
        let meanData = mean(data: data)
        var sumOfSquares : [String : Double] = [:]
        var std : [String : Double] = [:]

        let str = "xyz"
        for char in str {
             sumOfSquares[String(char)] = 0
            for i in data {
                let value = i[String(char)] as! Double
                sumOfSquares[String(char)]! += pow(( value - meanData[String(char)]!),2)
            }
            std[String(char)] = sqrt(sumOfSquares[String(char)]! / (Double(data.count - 1)))
        }
        return std
    }
    
}
